[{
        "name": "Motosserra T435",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20T435%20%28966997212%29%20-%202018-10.pdf"
    },
    {
        "name": "Motosserra 120",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20120%20%28967796301%29.pdf"
    },
    {
        "name": "Motosserra 125",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20125%20%28967796401%29.pdf"
    },
    {
        "name": "Motosserra 435 II",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20435%20II%20%28967651701%29.pdf"
    },
    {
        "name": "Motosserra 445 II",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20445%20II%20%28967156438%29.pdf"
    },
    {
        "name": "Motosserra 353",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20353%20%28965169718%29%20-%202016-12.pdf"
    },
    {
        "name": "Motosserra 61",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%2061%20%28967062433%29.pdf"
    },
    {
        "name": "Motosserra 272XP®",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20272XP%20%28965681683%29.pdf"
    },
    {
        "name": "Motosserra 372XP®",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20372%20XP%20%28965702688%29.pdf"
    },
    {
        "name": "Motosserra 281XP®",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20281XP%20%28965801490%29%20-%202018-12.pdf"
    },
    {
        "name": "Motosserra 288XP®",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20288XP%20%28965820488%29%20-%202018-12.pdf"
    },
    {
        "name": "Motosserra 395XP®",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20395%20XP%20%28965902428%29.pdf"
    },
    {
        "name": "Roçadeira 131R",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20131R%20%28967843102%29.pdf"
    },
    {
        "name": "Roçadeira 531RB",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20531RB%20%28967932901%29.pdf"
    },
    {
        "name": "Roçadeira 226R",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20226R%20%28965871001%29.pdf"
    },
    {
        "name": "Roçadeira 226K",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20226K%20%28967984501%29.pdf"
    },
    {
        "name": "Roçadeira 236R",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20236R%20%28967333003%29.pdf"
    },
    {
        "name": "Roçadeira 531RS",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20531RS%20%28967660601%29.pdf"
    },
    {
        "name": "Roçadeira 143 RII",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20143RII%20%28967332904%29.pdf"
    },
    {
        "name": "Roçadeira 541RS",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20541RS%20%28967660802%29.pdf"
    },
    {
        "name": "Roçadeira 541RST",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20541RST%20%28967660901%29.pdf"
    },
    {
        "name": "Roçadeira 545FR",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20545FR%20%28967637901%29.pdf"
    },
    {
        "name": "Multifuncional 129LK",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20129LK%20%28967193602%29%20-%202018-03.pdf"
    },
    {
        "name": "Multifuncional 525LK",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20525LK%20%28967148302%29.pdf"
    },
    {
        "name": "Podador de cerca viva 122HD60",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20122HD60%20%28966532401%29.pdf"
    },
    {
        "name": "Podador de cerca viva 325HE4",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20325HE4%20%28966787701%29.pdf"
    },
    {
        "name": "Podador e galho 525P5S",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20525P5S%20%28967329501%29.pdf"
    },
    {
        "name": "Soprador 125B",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20125B%20%28952715643%29.pdf"
    },
    {
        "name": "Soprador 125BVx",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20125BVx%20%28952715645%29.pdf"
    },
    {
        "name": "Soprador 350BT",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20350BT%20%28965877501%29.pdf"
    },
    {
        "name": "Soprador 570BTS",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20570BTS%20%28966629401%29.pdf"
    },
    {
        "name": "Pulverizador 321S15",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20321S15%20%28967078502%29.pdf"
    },
    {
        "name": "Pulverizador 321S25",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20321S25%20%28967078602%29.pdf"
    },
    {
        "name": "Pulverizador 325S25",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20325S25%20%28966746501%29.pdf"
    },
    {
        "name": "Atomizador 362M18",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20362M18%20%28966708101%29.pdf"
    },
    {
        "name": "Atomizador 362D28",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20362D28%20%28966708201%29.pdf"
    },
    {
        "name": "Motobomba W40P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20W40P%20%28967638902%29.pdf"
    },
    {
        "name": "Motobomba W50P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20W50P%20%28967639002%29.pdf"
    },
    {
        "name": "Motobomba W80P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20W80P%20%28967639102%29.pdf"
    },
    {
        "name": "Motobomba W100D",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20W100D%20%28967639201%29.pdf"
    },
    {
        "name": "Gerador G1300P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20G1300P%20%28967664903%29.pdf"
    },
    {
        "name": "Gerador G2500P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20G2500P%20%28967665003%29.pdf"
    },
    {
        "name": "Gerador G3200P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20G3200P%20%28967665103%29.pdf"
    },
    {
        "name": "Gerador G5500P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20G5500P%20%28967665203%29.pdf"
    },
    {
        "name": "Cortador de Grama LC140",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20LC140%20%28967636702%29.PDF"
    },
    {
        "name": "Ccortador de Grama GX560",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20%28BR%29%20-%20GX560%20%28960400602%29.pdf"
    },
    {
        "name": "Ccortador de Grama LC153P",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20LC153P%20%28967988502%29.pdf"
    },
    {
        "name": "Ccortador de Grama LC153S",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20LC153S%20%28967988602%29.pdf"
    },
    {
        "name": "Ccortador de Grama LB256S",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/OM%20-%20LB256S%20%28967988702%29.pdf"
    },
    {
        "name": "Motosserra 120i",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/120i%2C%20OM.pdf"
    },
    {
        "name": "Soprador 320iB",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/320iB%2C%20OM.pdf"
    },
    {
        "name": "Aparador de grama 115iL",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/115iL%2C%20OM.pdf"
    },
    {
        "name": "Podador de cerca viva 115iHD45",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/115iHD45%2C%20OM.pdf"
    },
    {
        "name": "Motosserra 535iXP",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/535i%20XP%2C%20OM.pdf"
    },
    {
        "name": "Roçadeira 520iRX",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/520iRX%2C%20OM.pdf"
    },
    {
        "name": "Podador de Galhos 530iPT4",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/530iP4%2C%20530iPT5%2C%20OM.pdf"
    },
    {
        "name": "Podador de cerca viva",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/520iHD60%2C%20OM.pdf"
    },
    {
        "name": "Podador de cerca viva articulado 520iHE3",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/520iHE3%2C%20OM.pdf"
    },
    {
        "name": "Soprador 525iB",
        "url": "https://api.husqvarna.mysanders.com.br/pdf/525iB%2C%20OM.pdf"
    }
]