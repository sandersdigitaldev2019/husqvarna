$(document).ready(function () {
  if ("onhashchange" in window) {
    var hash = window.location.hash;
    console.log(hash);
    $(".step_bar__item").removeClass("actived");
    $(".step_bar__item").removeClass("older");
    if (hash == "#/cart") {
      $(".step_bar__item--one").addClass("actived");
    }
    if (hash == "#/email") {
      $(".step_bar__item--two").addClass("actived");
      $(".step_bar__item--one").addClass("older");
    }
    if (hash == "#/profile" || hash == "#/shipping") {
      $(".step_bar__item--three").addClass("actived");
      $(".step_bar__item--one").addClass("older");
      $(".step_bar__item--two").addClass("older");
    }
    if (hash == "#/payment") {
      $(".step_bar__item--four").addClass("actived");
      $(".step_bar__item--one").addClass("older");
      $(".step_bar__item--two").addClass("older");
      $(".step_bar__item--three").addClass("older");
    }
  }
  $(window).on("hashchange", function (e) {
    var hash = window.location.hash;
    console.log(hash);
    $(".step_bar__item").removeClass("actived");
    $(".step_bar__item").removeClass("older");
    if (hash == "#/cart") {
      $(".step_bar__item--one").addClass("actived");
    }
    if (hash == "#/email") {
      $(".step_bar__item--two").addClass("actived");
      $(".step_bar__item--one").addClass("older");
    }
    if (hash == "#/profile" || hash == "#/shipping") {
      $(".step_bar__item--three").addClass("actived");
      $(".step_bar__item--one").addClass("older");
      $(".step_bar__item--two").addClass("older");
    }
    if (hash == "#/payment") {
      $(".step_bar__item--four").addClass("actived");
      $(".step_bar__item--one").addClass("older");
      $(".step_bar__item--two").addClass("older");
      $(".step_bar__item--three").addClass("older");
    }
  });
});
