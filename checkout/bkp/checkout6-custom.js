$(document).ready(function () {
  //ADICIONA TEXTO DO IBAMA SE TIVER MOTOSSERRA NO CARRINHO
  var ibamaText = function () {
    const products = document.querySelectorAll(".cart-template .cart .product-name a");

    for (let i = 0; i < products.length; i++) {
      if (products[i].textContent.includes("Motosserra") === true) {
        let text = products[i].textContent.toString();
        let newText = "<div class='ibama-text'><img style='margin-top: 5px' src='/arquivos/msgMotoserra.png'></div>";
        products[i].innerHTML = text + newText;
      } else {
        let text = products[i].textContent.toString();
        products[i].innerHTML = text;
      }
    }
  };

  //ADICIONA BANNER DE PROMO NO CARRINHO SE O CEP FOR DO PARANÁ
  /*var paranaSale = function() {
      var cepText = $('.srp-address-title').html();
      console.log('teste')
      if(cepText != undefined){
          console.log(cepText)
          $('header .promo-pr').remove();
          var cepNumber = Number(cepText.replace(/[^\d]+/g, ''))
          if(cepNumber >= 80000000 && cepNumber <= 87999999){
              $('header').prepend('<div class="promo-pr"></div>')
              var content = "<p class='main-text'>Aproveite agora o <span class='' style='color:#ff6731'>CUPOM* 'PR50'</span> e GANHE R$50,00 DE DESCONTO E <span class='' style='color:#ff6731'>FRETE GRÁTIS</span> nas compras acima de R$500,00</p><p class='note-text'>*Válido somente para o Estado do Paraná *Não é válido para produtos da linha Construção</p>";
              $('.promo-pr').html(content)
              setInterval(() => {
                  var blinkText = ($('.blink-text').css('opacity') == 1) ? 0 : 1;
                  $('.blink-text').css('opacity', blinkText);
              }, 800);
          }
      }
    }*/

  //TERMOS DE USO PARA PESSOA JÚRIDICA
  var termos_uso = function () {
    //OPEN
    $("#is-corporate-client").on("click", function () {
      $("#overlay, #termos_uso").addClass("active");
    });

    //CLOSE')
    $("#termos_uso .closed").on("click", function (e) {
      e.preventDefault();
      $("#overlay, #termos_uso").removeClass("active");
    });
  };

  termos_uso();

  document.addEventListener("readystatechange", () => {
    if (document.readyState == "complete" || document.readyState == "interactive") {
      ibamaText();
      paranaSale();
    }
  });
  $(window).on("orderFormUpdated.vtex", function (e, data) {
    setTimeout(() => {
      paranaSale();
    }, 850);
  });
});
