$mobile = 812;
$url = window.location.origin;

$(".helperComplement ").remove();

var header = {
  Accept: "application/json",
  "REST-range": "resources=0-1000",
  "Content-Type": "application/json; charset=utf-8",
};

var insertMasterData = function (ENT, loja, dados, fn) {
  $.ajax({
    url: "/api/dataentities/" + ENT + "/documents",
    type: "PATCH",
    data: dados,
    headers: header,
    success: function (res) {
      fn(res);
    },
    error: function (res) {
      swal("Oops!", "Houve um problema. Tente novamente mais tarde.", "error");
    },
  });
};

var set_cookie = function (cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

var get_cookie = function (cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};

var format_real = function (int) {
  var tmp = int + "";
  tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
  if (tmp.length > 6) tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
  return tmp;
};

var geral = (function () {
  var ambos = {
    barra_frete: function (value) {
      $(window).on("orderFormUpdated.vtex", function (evt, orderForm) {
        //HÝ PRODUTOS?
        if (orderForm.items.length === 0) {
          //MENSAGEM INICIAL
          $("#barra_frete .message").html(
            "Seu carrinho est\u00e1 vazio, acumulando R$ " +
              format_real(value) +
              " reais em compra, o <strong>FRETE SER\u00c3O GRATUITO!</strong>"
          );
          $("#barra_frete .status > span").css("width", 0);
        } else {
          //PREÇO TOTAL
          let price = orderForm.value >= value ? value : orderForm.value;
          $("#barra_frete .message").html(
            "Com mais R$ " +
              format_real(value - price) +
              " o <strong>FRETE \u00c9 POR NOSSA CONTA!</strong>"
          );

          //STATUS
          let percent = (price.toFixed(0) * 100) / value.toFixed(0);
          $("#barra_frete .status > span").css("width", percent + "%");

          //ATINGIU
          if (price === value) {
            $("#barra_frete .message").html(
              "Parab\u00e9ns, voc\u00ea ganhou <strong>FRETE GR\u00c1TIS!</strong>"
            );
          }
        }
      });
    },

    anchor: function () {
      $("a.anchor").on("click", function (event) {
        event.preventDefault();
        $("html, body").animate(
          {
            scrollTop: $($(this).attr("href")).offset().top,
          },
          500
        );
      });
    },

    mask: function () {
      $('input[data-name="cep"]').mask("00000-000");
      $('input[name="telefone"]').mask("(00) 0000-0000");
    },

    cep: function () {
      if (!get_cookie("cep")) {
        $(".pop_up.cep, #overlay, body").addClass("active");

        $("#your_cep").submit(function (event) {
          event.preventDefault();

          if ($("#your_cep input[name='cep']").val().length === 9) {
            $('#your_cep input[type="submit"]').val("Aguarde...");

            var items = [
              {
                id: 5,
                quantity: 1,
                seller: "1",
              },
            ];

            var postalCode = $(this).find('input[name="cep"]').val();

            vtexjs.checkout
              .simulateShipping(items, postalCode, "BRA")
              .done(function (result) {
                console.log(result);

                if (result.messages.length != 0) {
                  let cannotBeDelivered =
                    result.messages[0].code === "cannotBeDelivered";

                  //ATENDEMOS ESSE CEP?
                  if (cannotBeDelivered === true) {
                    $(".pop_up.cep").removeClass("active");
                    $(".pop_up.email").addClass("active");
                  }
                } else {
                  //ATUALIZA CEP NO CHECKOUT
                  vtexjs.checkout
                    .getOrderForm()
                    .then(function (orderForm) {
                      var address = {
                        postalCode: postalCode,
                        country: "BRA",
                      };
                      return vtexjs.checkout.calculateShipping(address);
                    })
                    .done(function (orderForm) {
                      //OK
                      console.log("CEP atualizado no Checkout.");

                      $('#your_cep input[type="submit"]').val("Enviar");
                      set_cookie(
                        "cep",
                        $('#your_cep input[name="cep"]').val(),
                        1
                      );
                      $(".pop_up.cep, #overlay, body").removeClass("active");
                    });
                }
              });
          } else if ($("#your_cep input[name='cep']").val().length === 0) {
            swal("Oops!", "Digite seu CEP!", "warning");
          } else {
            swal("Oops!", "CEP Inválido!", "error");
          }
        });
      }
    },

    //CASO CEP NÃO SEJA ATENDIDO
    email: function () {
      $("#your_email").submit(function (event) {
        event.preventDefault();

        $('#your_email input[type="submit"]').val("Aguarde...");

        let obj = {
          email: $('#your_email input[name="email"]').val(),
          cep: $('#your_cep input[name="cep"]').val(),
        };

        let json = JSON.stringify(obj);

        insertMasterData("CN", "husqvarna", json, function (res) {
          $(".pop_up.email, #overlay, body").removeClass("active");
          set_cookie("cep", $('#your_cep input[name="cep"]').val(), 2);
          swal(
            "E-mail cadastrado!",
            "Fique a vontade para navegar e conhecer nossas solu\u00e7\u00f5es.",
            "success"
          );
        });
      });
    },

    client_name: function () {
      $.getJSON("/no-cache/profileSystem/getProfile", function (res) {
        var name = res.IsUserDefined ? res.FirstName : null;
        if (name != null) {
          $("header #client_name").text(name);
        }
      });
    },

    busca: function () {
      $("header #busca input").on("input", function () {
        texto = $(this).val();

        if (texto != "") {
          $("header").addClass("active");
          $("header #busca, #overlay").addClass("active");

          $.ajax({
            type: "GET",
            dataType: "json",
            url: "/api/catalog_system/pub/products/search/" + texto,
          }).done(function (success) {
            if (success.length != 0) {
              var busca = "";

              $(success).each(function (index, a) {
                let categorias = a.categories[1].replace(
                  /[0-9`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,
                  ""
                );
                $("header #busca + .result .view").attr("href", categorias);
                $("header #busca + .result .digitando").html(
                  "<p>Buscar " +
                    texto +
                    ' em <a href="' +
                    categorias +
                    '">' +
                    categorias +
                    "</a></p>"
                );

                vtexjs.catalog
                  .getProductWithVariations(a.productId)
                  .done(function (product) {
                    if (product.available === true) {
                      $(product.skus).each(function (index, b) {
                        if (b.available === true) {
                          let ID = a.productId;
                          let nome = a.productName;
                          let foto = a.items[0].images[0].imageUrl;
                          let url = a.link;
                          let de_price = b.listPriceFormated;
                          let por_price = b.bestPrice;

                          if (por_price != 0) {
                            busca += "<li data-id=" + ID + ">";
                            busca += '<a href="' + url + '">';
                            busca +=
                              '<div class="image"><img src="' +
                              foto +
                              '" /></div>';
                            busca += '<div class="content">';
                            busca += '<h3 class="name">' + nome + "</h3>";
                            if (de_price != por_price) {
                              busca +=
                                '<p class="price">R$ ' +
                                format_real(por_price) +
                                "</p>";
                            } else {
                              busca +=
                                '<p class="price">R$ ' + por_price + "</p>";
                            }
                            busca += "</div>";
                            busca += "</li>";

                            $("header #busca + .result ul").html(busca);
                          }
                        }
                      });
                    }
                  });
              });
            }
          });
        } else {
          $("header #busca, #overlay").removeClass("active");
          $("header").removeClass("active");
          $(
            "header #busca + .result li, header #busca + .result .digitando p"
          ).remove();
        }
      });

      //SLICK ARROW
      var ul = $("header .result .slider ul");
      $(document).on("click", "header .slick-arrow.slick-prev", function () {
        $(ul).animate({
          scrollLeft: $(ul).scrollLeft() - 350,
        });
      });

      $(document).on("click", "header .slick-arrow.slick-next", function () {
        $(ul).animate({
          scrollLeft: $(ul).scrollLeft() + 350,
        });
      });

      //BUSCA - SUBMIT
      $("header #busca").submit(function (event) {
        event.preventDefault();
        location.href = "/" + $("header #busca input").val();
      });

      //CLOSE
      $("header #busca .close").on("click", function () {
        $("header #busca").trigger("reset");
        $("header #busca, #overlay").removeClass("active");
        $("header").removeClass("active");
        $(
          "header #busca + .result li, header #busca + .result .digitando p"
        ).remove();
      });
    },

    prateleira: function () {
      if($('body').hasClass('home')){
        if ($("main .container .row:nth-child(3) .prateleira ul li").length > 4) {
          //DESKTOP
          $("main .container .row:nth-child(3) .prateleira ul").slick({
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: true,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 2,
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  arrows: false
                },
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  arrows: false
                },
              },
            ],
          });
        }
        if ($("main .container .row:nth-child(4) .prateleira ul li").length > 2) {
          //DESKTOP
          $("main .container .row:nth-child(4) .prateleira ul").slick({
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: true,
            dots: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [
              {
                breakpoint: 812,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  arrows: false
                },
              }
            ],
          });
        }
      }else{
        if ($("body").width() > $mobile) {
          if ($(".prateleira ul li").length > 4) {
            //DESKTOP
            $("body:not(.departamento) .prateleira:not(.compre_junto) ul").slick({
              infinite: true,
              autoplay: true,
              autoplaySpeed: 4000,
              arrows: true,
              dots: false,
              slidesToShow: 4,
              slidesToScroll: 1,
            });
          }
        } else {
          //MOBILE
          $("body:not(.departamento) .prateleira:not(.compre_junto) ul").slick({
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
          });
        }
      }
    },

    newsletter: function () {
      $("#newsletterButtonOK").val("Enviar");
    },

    //FILTROS - DEPARTAMENTO
    overlay: function () {
      $("#overlay").on("click", function () {
        $(
          ".departamento .navigation-tabs.active, .departamento .orderBy.active, .departamento #overlay, body.departamento"
        ).removeClass("active");
      });
    },

    popupBF: function () {
      $(
        ".banners.three .box-banner:nth-child(2), .home main .container .row:nth-child(4) a, .full_banner .slick-slide:nth-child(1)"
      ).click(function (e) {
        e.preventDefault();
        $(".popup_bf").addClass("active");
        $("#overlay").addClass("active");
      });

      $(".popup_bf .close, #overlay").click(function (e) {
        $(".popup_bf").removeClass("active");
        $("#overlay").removeClass("active");
      });

      $('.popup_bf input[type="submit"]').click(function (e) {
        var nome = $(".popup_bf .form input:nth-child(1)").val();
        var cel = $(".popup_bf .form input:nth-child(2)").val();
        var email = $(".popup_bf .form input:nth-child(3)").val();
        var cidade = $(".popup_bf .form input:nth-child(4)").val();

        let obj = {
          nome: nome,
          email: email,
          celular: cel,
          cidade: cidade,
        };

        let json = JSON.stringify(obj);
        if (nome != "" && cel != "" && email != "" && cidade != "") {
          insertMasterData("AM", "husqvarna", json, function (res) {
            $(".popup_bf").removeClass("active");
            $("#overlay").removeClass("active");
            swal(
              "Cadastro realizado com sucesso!",
              "Voc\u00ea receber\u00e1 o voucher por e-mail em instantes.",
              "success"
            );
          });
        } else {
          swal(
            "Oops!",
            "Houve um problema. Tente verificar todos os campos",
            "error"
          );
        }
      });
    },

    popupBFinit: function (inicio, fim) {
      var dateNow = new Date().getTime();
      var dateInit = new Date(inicio).getTime();
      var faltando = dateInit - dateNow;
      var dateExit = new Date(fim).getTime();
      var check = setInterval(function () {
        dateNow = new Date().getTime();
        if (faltando < 300000) {
          if (dateNow >= dateInit && dateNow <= dateExit) {
            ambos.popupBF();
            clearInterval(check);
          } else if (dateNow > dateExit) {
            clearInterval(check);
          }
        } else {
          clearInterval(check);
        }
      }, 1000);
    },

    flag_brinde: function () {
      $(".prateleira article").each(function (index, element) {
        var prodId = $(this).data("id");
        $.ajax({
          url: "/api/catalog_system/pub/products/search?fq=productId:" + prodId,
          type: "GET",
        }).done(function (response, status) {
          if (response[0].productClusters[141]) {
            $(element).append(
              '<img src="/arquivos/selo_brinde.png" alt="Flag Brinde" style="position:absolute; right:15px; top:15px; width: 80px">'
            );
          }
        });
      });
    },
  };

  var desktop = {
    menu_banners: function () {
      $("header .full_menu nav>ul>li .submenu .content li a")
        .mouseenter(function () {
          let text = $(this).text();

          $("header .full_menu nav>ul>li .submenu .image img").addClass(
            "dis-none"
          );
          $(
            'header .full_menu nav>ul>li .submenu .image img[alt="' +
              text +
              '"]'
          ).removeClass("dis-none");
        })
        .mouseleave(function () {
          $("header .full_menu nav>ul>li .submenu .image img").addClass(
            "dis-none"
          );
          $(
            "header .full_menu nav>ul>li .submenu .image img:nth-child(1)"
          ).removeClass("dis-none");
        });
    },
  };

  ambos.barra_frete(50000); //R$ 500,00
  ambos.anchor();
  ambos.mask();
  // ambos.cep();
  ambos.email();
  ambos.client_name();
  ambos.busca();
  ambos.prateleira();
  ambos.newsletter();
  ambos.overlay();
  // ambos.popupBFinit('December 15, 2020 00:00:01', 'December 24, 2020 23:59:59');
  // ambos.flag_brinde();
  // ambos.popupBF();

  var mobile = {
    hamburger: function () {
      $("#hamburger").on("click", function (event) {
        event.preventDefault();
        $("header .full_menu, #overlay, body").addClass("active");
      });
    },

    close: function () {
      $(".full_menu .close").on("click", function (event) {
        event.preventDefault();
        $("header .full_menu, #overlay, body").removeClass("active");
      });
    },

    full_menu: function () {
      $("header .row_2 .full_menu .content nav>ul>li").on(
        "click",
        function (event) {
          if ($(this).find("a").next(".submenu").length) {
            // event.preventDefault();

            if ($(this).hasClass("active")) {
              //CLOSE
              $(this).removeClass("active");

              $(this).next(".submenu").removeClass("active");
              $(this).next(".submenu").slideUp();
            } else {
              $("header .row_2 .full_menu .content nav>ul>li").removeClass(
                "active"
              );
              $(
                "header .row_2 .full_menu .content nav>ul>li .submenu"
              ).slideUp();

              //OPEN
              $(this).addClass("active");

              $(this).next(".submenu").addClass("active");
              $(this).next(".submenu").slideDown();
            }
          }
        }
      );
    },

    search: function () {
      $("header .search > a").on("click", function (event) {
        event.preventDefault();
        $("header #busca").toggleClass("show");
        $("#overlay").toggleClass("active");
      });
    },

    tipbar: function () {
      $(".tipbar ul").slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        responsive: [
          {
            breakpoint: 812,
            settings: {
              variableWidth: false,
            },
          }
        ],
      });
    },
  };

  if ($("body").width() < $mobile) {
    mobile.hamburger();
    mobile.close();
    mobile.full_menu();
    mobile.search();
    mobile.tipbar();
  } else {
    //ONLY DESKTOP
    desktop.menu_banners();
  }
})();

var carrinho = (function () {
  var geral = {
    toggle_carrinho: function () {
      $("header #cart").on("click", function (event) {
        event.preventDefault();
        $("#cart-lateral, #overlay").addClass("active");
      });

      $("#cart-lateral .header .close").on("click", function (event) {
        event.preventDefault();
        $("#cart-lateral, #overlay").removeClass("active");
      });
    },
  };

  var desktop = {
    calculateShipping: function () {
      if ($('#search-cep input[type="text"]').val() != "") {
        vtexjs.checkout
          .getOrderForm()
          .then(function (orderForm) {
            if (localStorage.getItem("cep") === null) {
              var postalCode = $('#search-cep input[type="text"]').val();
            } else {
              var postalCode = localStorage.getItem("cep");
            }

            var country = "BRA";
            var address = {
              postalCode: postalCode,
              country: country,
            };

            return vtexjs.checkout.calculateShipping(address);
          })
          .done(function (orderForm) {
            if (orderForm.totalizers.length != 0) {
              var value_frete = orderForm.totalizers[1].value / 100;
              value_frete = value_frete.toFixed(2).replace(".", ",").toString();
              $("#cart-lateral .value-frete").text("R$: " + value_frete);

              var postalCode = $('#search-cep input[type="text"]').val();
              localStorage.setItem("cep", postalCode);
              $('#search-cep input[type="text"]').val(postalCode);
            }
          });
      }
    },

    cartLateral: function () {
      vtexjs.checkout.getOrderForm().done(function (orderForm) {
        //REMOVE LOADING
        $("#cart-lateral .columns").removeClass("loading");

        //TOTAL CARRINHO
        var quantidade = 0;
        for (var i = orderForm.items.length - 1; i >= 0; i--) {
          quantidade =
            parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
        }

        $("header #cart span").text(quantidade);

        //INFORMACOES DO CARRINHO
        if (orderForm.value != 0) {
          total_price = orderForm.value / 100;
          total_price = total_price.toFixed(2).replace(".", ",").toString();

          $("#cart-lateral .footer .total-price").text("R$: " + total_price);
        } else {
          $("#cart-lateral .footer .total-price").text("R$: 0,00");
        }

        if (orderForm.totalizers.length != 0) {
          sub_price = orderForm.totalizers[0].value / 100;
          sub_price = sub_price.toFixed(2).replace(".", ",").toString();

          $(
            "#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total"
          ).text("R$: " + sub_price);

          //FRETE
          if (orderForm.totalizers[1]) {
            $("#cart-lateral .entrega .value").html(
              "R$ " + format_real(orderForm.totalizers[1].value)
            );
          }
        } else {
          $(
            "#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total"
          ).text("R$: 0,00");
        }

        if (orderForm.items != 0) {
          total_items = orderForm.items.length;

          $("#cart-lateral .header .total-items span").text(
            total_items + " Itens"
          );
        } else {
          $("#cart-lateral .header .total-items span").text("0 Itens");
          $("#cart-lateral .footer .entrega .value").html(
            '<a href="/checkout" title="Calcular">Calcular</a>'
          );
        }
        //FIM - INFORMACOES DO CARRINHO

        //ITEMS DO CARRINHO
        $("#cart-lateral .content ul li").remove();
        for (i = 0; i < orderForm.items.length; i++) {
          price_item = orderForm.items[i].price / 100;
          price_item = price_item.toFixed(2).replace(".", ",").toString();

          var content = "";

          content += '<li data-index="' + i + '">';
          content +=
            '<div class="column_1"><img src="' +
            orderForm.items[i].imageUrl +
            '" alt="' +
            orderForm.items[i].name +
            '"/></div>';

          content += '<div class="column_2">';
          content += '<div class="name">';
          content += "<p>" + orderForm.items[i].name + "</p>";
          content += "</div>";

          content += '<div class="ft">';
          content += "<ul>";
          content += '<li class="price">';
          content += "<p>R$: " + price_item + "</p>";
          content += "</li>";
          content += '<li data-index="' + i + '">';
          content += '<div class="box-count">';
          content += '<a href="" class="count count-down">- 1</a>';
          content +=
            '<input type="text" value="' + orderForm.items[i].quantity + '" />';
          content += '<a href="" class="count count-up">+ 1</a>';
          content += "</div>";
          content += "</li>";
          content += "<ul>";

          content += "</div>";
          content += "</div>";

          content +=
            '<span class="removeUni"><img src="/arquivos/ico-trash.png" alt="Remover Produto"/></span>';
          content += "</li>";

          $("#cart-lateral .content > ul").append(content);
        }
        //FIM - ITEMS DO CARRINHO
      });
    },

    changeQuantity: function () {
      $(document).on("click", "#cart-lateral .count", function (e) {
        e.preventDefault();

        $("#cart-lateral .columns").addClass("loading");

        var qtd = $(this).siblings('input[type="text"]').val();
        if ($(this).hasClass("count-up")) {
          qtd++;
          $(this).siblings('input[type="text"]').removeClass("active");
          $(this).siblings('input[type="text"]').val(qtd);
        } else if ($(this).hasClass("count-down")) {
          if ($(this).siblings('input[type="text"]').val() != 1) {
            qtd--;
            $(this).siblings('input[type="text"]').val(qtd);
          } else {
            //ALERTA 0 USUARIO QUANTIDADE NEGATIVA
            $(this).siblings('input[type="text"]').addClass("active");
          }
        }

        var data_index = $(this).parents("li").data("index");
        var data_quantity = $(this)
          .parents("li")
          .find('.box-count input[type="text"]')
          .val();

        vtexjs.checkout.getOrderForm().then(function (orderForm) {
          var total_produtos = parseInt(orderForm.items.length);
          vtexjs.checkout
            .getOrderForm()
            .then(function (orderForm) {
              var itemIndex = data_index;
              var item = orderForm.items[itemIndex];

              var updateItem = {
                index: data_index,
                quantity: data_quantity,
              };

              return vtexjs.checkout.updateItems([updateItem], null, false);
            })
            .done(function (orderForm) {
              desktop.cartLateral();
            });
        });
      });
    },

    removeItems: function () {
      $(document).on("click", "#cart-lateral .removeUni", function () {
        var data_index = $(this).parents("li").data("index");
        var data_quantity = $(this)
          .siblings("li")
          .find('.box-count input[type="text"]')
          .val();

        vtexjs.checkout
          .getOrderForm()
          .then(function (orderForm) {
            var itemIndex = data_index;
            var item = orderForm.items[itemIndex];
            var itemsToRemove = [
              {
                index: data_index,
                quantity: data_quantity,
              },
            ];
            return vtexjs.checkout.removeItems(itemsToRemove);
          })
          .done(function (orderForm) {
            desktop.cartLateral();
          });
      });
    },

    removeAllItems: function () {
      $("#cart-lateral .clear").on("click", function () {
        vtexjs.checkout.removeAllItems().done(function (orderForm) {
          //ATUALIZA O CARRINHO AP�S ESVAZIAR
          desktop.cartLateral();
        });
      });
    },

    btn_buy: function () {
      window.alert = function () {
        //OPEN CART
        $("header .cart-container a").trigger("click");
        desktop.cartLateral();
      };
    },

    openCart: function () {
      $("#overlay").on("click", function () {
        if ($("#cart-lateral").hasClass("active")) {
          $("#cart-lateral, #overlay").removeClass("active");
        }
      });
    },
  };

  var produto = {
    buy_in_page: function (departamento) {
      window.alert = function () {
        if (skuJson.productId != 199 && skuJson.productId != 200) {
          // ROBOS CORTADORES DE GRAMA
          // if (departamento === "PRODUTOS A BATERIA") {
          //     //AVISO SOBRE A COMPRA
          //     $(
          //         'body, #overlay, .pop_up[data-departamento="' +
          //         departamento +
          //         '"]'
          //     ).addClass("active");
          // } else if (
          //     departamento === "ACESS\u00d3RIOS" ||
          //     departamento === "FOR\u00c7A E ENERGIA" ||
          //     departamento === "CORTE DE GRAMA"
          // ) {
          //     //ACESSÓRIOS, CORTE DE GRAMA, FORÇA E ENERGIA (MOTOBOMBA, GERADORES)
          //     $("header #cart").trigger("click");
          // } else {
          //     //PRODUTOS A COMBUSTÃO - EXCETO /\
          //     $('body, #overlay, .pop_up[data-departamento="PRODUTOS A COMBUST\u00e3O"]').addClass("active");
          // }

          if (
            departamento === "PRODUTOS A BATERIA" ||
            departamento === "PRODUTOS A COMBUSTÃO"
          ) {
            //AVISO SOBRE A COMPRA
            $(
              'body, #overlay, .pop_up[data-departamento="' +
                departamento +
                '"]'
            ).addClass("active");
          } else {
            $("header #cart").trigger("click");
          }
        } else {
          $("header #cart").trigger("click");
        }

        //ATUALIZA CARRINHO
        desktop.cartLateral();
      };
    },
  };

  geral.toggle_carrinho();

  desktop.cartLateral();
  desktop.changeQuantity();
  desktop.removeItems();
  desktop.removeAllItems();
  desktop.btn_buy();
  desktop.openCart();

  if ($("body").hasClass("produto")) {
    produto.buy_in_page(vtxctx.departmentName);
  }
})();

var home = (function () {
  var desktop = {
    banner_principal: function () {
      $(".home .full_banner .desktop ul").slick({
        dots: false,
        arrows: true,
        autoplay: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 4000,
        appendArrows: ".home .full_banner .arrows .container",
      });
    },
  };

  var mobile = {
    banner_principal: function () {
      $(".home .full_banner .mobile ul").slick({
        dots: false,
        arrows: true,
        autoplay: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 4000,
      });
    },
  };

  var ambos = {
    choice: function () {
      $(".home .choice ul").slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        arrows: true,
        dots: true,
        slidesToShow: 5,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });
    },

    instagram: function () {
      $.instagramFeed({
        username: "husqvarnabrasil",
        container: "#instagram",
        display_profile: false,
        display_biography: false,
        display_gallery: true,
        get_raw_json: true,
        callback: function (data) {
          data = $.parseJSON(data);
          let post = data.images;

          $.each(post, function (index, item) {
            let node =
              '<li><a href="https://www.instagram.com/p/' +
              item.node.shortcode +
              '" target="_blank"><img src="' +
              item.node.thumbnail_src +
              '"/></a></li>';

            if (index < 4) {
              $("#instagram .column.column_1 ul").append(node);
            } else if (index > 4 && index < 9) {
              $("#instagram .column.column_3 ul").append(node);
            }
          });
        },
        styling: false,
        items: 4,
        items_per_row: 4,
        margin: 0,
      });
    },

    blog: function () {
      $(".home #blog ul").slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        arrows: true,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });
    },

    flag_frete: function () {
      $(".prateleira article").each(function (index, element) {
        $(element).append(
          '<img src="/arquivos/flag_frete-gratis.png" alt="Flag frete grátis" style="position:absolute; right:15px; top:0px; width: 80px">'
        );
      });
    },
  };

  if ($("body").hasClass("home")) {
    if ($("body").width() < $mobile) {
      //MOBILE
      mobile.banner_principal();
    } else {
      //DESKTOP
      desktop.banner_principal();
    }

    // ambos.flag_frete();
    ambos.choice();
    ambos.instagram();
    ambos.blog();
  }
})();

var pop_up = (function () {
  var geral = {
    close: function () {
      $(".pop_up .close").on("click", function () {
        $("body.active, #overlay.active, .pop_up.active").removeClass("active");

        //ABRE CARRINHO
        if ($(this).parents(".pop_up").hasClass("compra")) {
          $("header #cart").trigger("click");
        }

        //CEP - EMAIL
        if (
          $(this).parents(".pop_up").hasClass("cep") ||
          $(this).parents(".pop_up").hasClass("email")
        ) {
          set_cookie("cep", "false", 1);
        }
      });
    },

    open: function () {
      $(".open_pop_up").on("click", function (event) {
        event.preventDefault();

        $("body,#overlay").addClass("active");
        $("article.pop_up." + $(this).data("popup")).addClass("active");
      });
    },
  };

  geral.close();
  geral.open();
})();

var produto = (function () {
  var ambos = {
    add_multiple_sku: function (arr) {
      var finalArr = arr.reduce((m, o) => {
        var found = m.find((p) => p.sku === o.sku);
        if (found) {
          found.quantity += o.quantity;
        } else {
          m.push(o);
        }
        return m;
      }, []);

      var addToCart = {
        index: 0,
        add: {
          products: function (itens, cb) {
            addToCart.products = addToCart.products || [];
            itens = itens[0][0].reverse();
            for (var i in itens) {
              if (itens.hasOwnProperty(i)) {
                addToCart.products.push({
                  id: itens[i].sku,
                  quantity: itens[i].quantity,
                  seller: itens[i].seller,
                });
              }
            }
            addToCart.index = addToCart.products.length - 1;
            addToCart.add.product(addToCart.products[addToCart.index], cb);
            return true;
          },
          product: function (item, cb) {
            var adding = false;
            if (
              typeof addToCart.products !== "undefined" &&
              addToCart.index < 0 &&
              typeof cb === "function"
            ) {
              addToCart.products = [];
              cb();
            }
            if (typeof item == "undefined") {
              return false;
            }
            var product = {
              id: item.id,
              quantity: 1 * item.quantity,
              seller: item.seller || 1,
            };
            var next = function () {
              addToCart.log(
                "Product id: " +
                  product.id +
                  ", quantity: " +
                  product.quantity +
                  " added."
              );
              if (typeof addToCart.products != "undefined") {
                addToCart.index--;
                addToCart.add.product(addToCart.products[addToCart.index], cb);
              }
            };
            if (!adding) {
              var add = function (prod) {
                var url =
                  "/checkout/cart/add?sku=" +
                  prod.id +
                  "&seller=1&redirect=false&qty=" +
                  prod.quantity;
                adding = true;
                $.get(url, function () {
                  adding = false;
                  next();
                });
              };
              vtexjs.checkout.getOrderForm().then(function (orderForm) {
                var found = false;
                var items = orderForm.items;
                if (
                  typeof orderForm != "undefined" &&
                  orderForm.items.length > 0
                ) {
                  for (var i in items) {
                    if (items.hasOwnProperty(i) && items[i].id == product.id) {
                      found = true;
                      (product.index = items[i].sku),
                        (product.quantity = items[i].quantity),
                        (product.seller = items[i].seller);
                    } else {
                      found = false;
                    }
                  }
                }

                add(product);
                console.log(product);
                return true;
              });
            }
            return true;
          },
        },
        log: function () {
          if (
            "undefined" == typeof console &&
            "undefined" == typeof arguments &&
            "undefined" == typeof console.log
          ) {
            return false;
          }
          for (var i in arguments) {
            console.log(arguments[i]);
          }
          return true;
        },
      };
      var addProducts = function (data, cb) {
        addToCart.add.products(data, cb);
        return true;
      };
      var addProduct = function (item, cb) {
        var data = [[item.id, item.quantity, item.seller]];
        addToCart.add.products(data, cb);
        return true;
      };

      addProducts([[finalArr]], function () {
        window.location.href = "/checkout";
        $('a[data-function="add-multiple-sku"]').removeClass("disabled");
      });
    },

    thumbs: function () {
      $(".produto .thumbs").slick({
        infinite: true,
        autoplay: false,
        autoplaySpeed: 4000,
        arrows: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              adaptiveHeight: true,
              dots: false,
            },
          },
        ],
      });

      $(".produto .thumbs .slick-dots li").each(function (index, item) {
        $(item).attr("data-index", index);
      });

      $('.produto .thumbs .slick-list li:not(".slick-cloned")').each(function (
        i,
        item
      ) {
        //VALIDA��O PARA VIDEO
        let index = $(item).parents(".slick-slide").attr("data-slick-index");

        let image = $(item).find("img").attr("src");

        //VIDEO
        if (image === undefined) {
          $(
            ".produto .thumbs .slick-dots li[data-index=" + index + "]"
          ).addClass("slick-video");
        }

        $(".produto .thumbs .slick-dots li[data-index=" + index + "]").css(
          "background-image",
          "url(" + image + ")"
        );
      });

      //ZOOM APENAS DESKTOP
      if ($("body").width() > $mobile) {
        $(".thumbs .slick-slide").zoom();
      }
    },

    parcelamento: function () {
      $(".titulo-parcelamento").on("click", function () {
        $(this).parents(".other-payment-method").toggleClass("active");
        $(this).next("ul").slideToggle();
      });
    },

    detalhes: function () {
      $(".produto .btn_detalhes").on("click", function () {
        $('.produto .tabs .options a[data-step="1"]').trigger("click");
      });
    },

    tabs: function () {
      $(".produto .tabs .options a").on("click", function (event) {
        event.preventDefault();

        $(".produto .tabs .options a, .produto .content .step").removeClass(
          "active"
        );

        $(this).addClass("active");
        $(".produto .content .step_" + $(this).data("step")).addClass("active");
      });
    },

    ficha_tecnica: function () {
      $("#caracteristicas table.group").each(function (index, item) {
        let name = $(item).attr("class").split(" ")[1];
        $(item).appendTo(
          '.produto .tabs .content .step.step_2 .list .box[data-name="' +
            name +
            '"]'
        );
        $(
          '.produto .tabs .content .step.step_2 .list .box[data-name="' +
            name +
            '"]'
        ).removeClass("dis-none");
      });

      if ($(".produto main .row_2 .tabs .content .step.step_2 table").length) {
        $(
          '.produto main .row_2 .tabs .options ul li a[data-step="2"]'
        ).removeClass("dis-none");
      }
    },

    video: function () {
      $.ajax({
        url:
          "/api/catalog_system/pub/products/search?fq=productId:" +
          skuJson.productId,
        type: "GET",
        headers: header,
      }).done(function (response) {
        let res = response[0];

        //THUMB
        let url = res["V\u00eddeo thumb"];
        if (url) {
          let video = "";
          video += "<li>";
          video += '<a href="#" title="V\u00eddeo" id="play_video">';
          video +=
            '<video id="video" autoplay loop muted playsinline preload="metadata" poster="https://husqvarna.vteximg.com.br/arquivos/ico-play.png" src="' +
            url +
            '" width="100%" height="480">';
          video += "<p>Seu navegador n\u00e3o suporte <video> HTML5.</p>";
          video += "</video>";
          video += "</a>";
          video += "</li>";

          $(".thumbs").append(video);
          ambos.thumbs();
        } else {
          ambos.thumbs();
        }

        $(document).on("click", ".thumbs li.slick-video", function () {
          let video = document.getElementById("video");
          video.load();
          video.play();
        });
        //THUMB

        //INSTRUÇÃO DE USO
        let url2 = res["V\u00eddeo Instru\347\365es"];
        if (url2) {
          $(".produto main .row_2 .tabs .content .step_4").append(
            '<iframe width="100%" height="400px" src="https://www.youtube.com/embed/' +
              url2 +
              '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen"></iframe>'
          );
        }
        //INSTRUÇÃO DE USO

        //APLICAÇÃO
        let url3 = res["V\u00eddeo Aplica\347\343o"];
        if (url3) {
          $(".produto main .row_2 .tabs .content .step_5").append(
            '<iframe width="100%" height="400px" src="https://www.youtube.com/embed/' +
              url3 +
              '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen"></iframe>'
          );
        }
        //APLICAÇÃO

        //CASO NÃO TENHA - OCULTAR OPÇÃO
        if ($(".produto main .row_2 .tabs .content .step_4").html() != "") {
          $(
            '.produto main .row_2 .tabs .options ul li a[data-step="4"]'
          ).removeClass("dis-none");
        }

        if ($(".produto main .row_2 .tabs .content .step_5").html() != "") {
          $(
            '.produto main .row_2 .tabs .options ul li a[data-step="5"]'
          ).removeClass("dis-none");
        }
      });
    },

    buttons: function (response) {
      //PDF
      if (vtxctx.departmentName != "ACESS\u00d3RIOS") {
        $(".produto .product_info .btn_pdf").removeClass("dis-none");
      }

      if (response[0].Manual) {
        $(".produto .product_info .btn_pdf").attr(
          "href",
          response[0].Manual[0]
        );
        console.log(response[0]);
      }
      //VALE A PENA INVESTIR EM BATERIA?
      // if (response[0].categoryId == 35) {
      if (response[0].categories[1] == "/PRODUTOS A BATERIA/") {
        $(
          '.produto .product_info .btn_bateria, .produto .tabs .options ul li a[data-departmentName="' +
            vtxctx.departmentName +
            '"]'
        ).removeClass("dis-none");
      }

      //TABS
      $(
        '.produto main .row_2 .tabs .options ul li a[data-categoryName="' +
          vtxctx.categoryName +
          '"]'
      ).removeClass("dis-none");
      console.log(response);
    },

    buttonSimulador: function () {
      if (skuJson.productId == 199 || skuJson.productId == 200) {
        $(".buy-in-page-button").css("display", "none");
        $(
          '<a href="" class="buy-button-custom-robo" style="background-color: #51c828;display: block;width: 100%;height: 55px;line-height: 55px;color: #fff;text-align: center;font-size: 22px;font-weight: 700;text-transform: uppercase;margin-bottom: 20px;">ADICIONAR AO CARRINHO</a>'
        ).insertAfter(".produto .product_info .product_payment");

        $(
          '<a href="/simulador" style="background-color: #273a60;display: block;width: 100%;height: 55px;line-height: 55px;color: #fff;text-align: center;font-size: 22px;font-weight: 700;text-transform: uppercase;margin-bottom: 20px;">INICIAR SIMULAÇÃO</a>'
        ).insertAfter(".produto .product_info .product_payment");
      }

      $(".buy-button-custom-robo").click(function (e) {
        e.preventDefault();
        $(
          'body, #overlay, .pop_up[data-departamento="ROBO CORTADOR DE GRAMA"]'
        ).addClass("active");
      });

      $(
        '.pop_up[data-departamento="ROBO CORTADOR DE GRAMA"] .popup_btn_buy'
      ).click(function (e) {
        e.preventDefault();
        $(".buy-in-page-button").trigger("click");
        $(
          'body, #overlay, .pop_up[data-departamento="ROBO CORTADOR DE GRAMA"]'
        ).removeClass("active");
        $("header #cart").trigger("click");
      });
    },

    acessorios: function () {
      if ($(".box_acessorios .cards_acessorios ul li").length != 0) {
        $(".box_acessorios fieldset").remove();
        $(".produto .box_acessorios").removeClass("dis-none");

        function slick(qtd) {
          $(".box_acessorios .cards_acessorios > ul").slick({
            infinite: false,
            autoplay: false,
            arrows: true,
            dots: false,
            slidesToShow: qtd,
            slidesToScroll: 1,
            autoplaySpeed: 3000,
          });
        }

        function screen() {
          if ($("body").width() > $mobile) {
            if ($(".box_acessorios .cards_acessorios ul li").length > 2) {
              slick(2);
            }
          } else {
            slick(1);
          }
        }
        screen();

        function title() {
          //TÝTULO
          $(".box_acessorios > h2").text(
            $(".box_acessorios .cards_acessorios > h2").text()
          );
        }
        title();

        //VALOR FINAL
        function total() {
          var total = 0;
          //VALOR: PRODUTO DA PÝGINA
          $(skuJson.skus).each(function (a, b) {
            if (b.available === true) {
              total = total = b.bestPrice;
              $(".box_acessorios .column_2 article p strong").text(
                "R$ " + format_real(b.bestPrice)
              );
            }
          });

          //VALOR: PRODUTOS ACESSÓRIOS
          $(".box_acessorios .cards_acessorios ul li.active article").each(
            function (a, b) {
              let data_id = $(b).data("id");
              vtexjs.catalog
                .getProductWithVariations(data_id)
                .done(function (product) {
                  total = total += product.skus[0].bestPrice;
                  $(".box_acessorios .column_2 article p strong").text(
                    "R$ " + format_real(total)
                  );
                });
            }
          );
        }
        total();

        function add() {
          $(".box_acessorios .cards_acessorios ul li article .add").on(
            "click",
            function (event) {
              event.preventDefault();

              $(this).toggleClass("active");
              $(this).parents("li").toggleClass("active");

              total();
            }
          );
        }
        add();

        function buy() {
          $(
            '.produto .box_acessorios .buy[data-function="add-multiple-sku"]'
          ).on("click", function (e) {
            e.preventDefault();

            //ALGUM SELECIONADO?
            if (
              $(".box_acessorios .cards_acessorios ul li.active").length != 0
            ) {
              $(this).addClass("disabled");

              //PRODUTO PRINCIPAL
              var arr = [
                {
                  sku: skuJson.skus[0].sku,
                  quantity: 1,
                  seller: "1",
                },
              ];

              $(".box_acessorios .cards_acessorios ul li.active article").each(
                function (index, item) {
                  vtexjs.catalog
                    .getProductWithVariations($(item).data("id"))
                    .done(function (product) {
                      arr.push({
                        sku: product.skus[0].sku,
                        quantity: 1,
                        seller: "1",
                      });
                    });
                }
              );

              ambos.add_multiple_sku(arr);
            } else {
              swal("Oops!", "Escolha um produto.", "warning");
            }
          });
        }
        buy();

        function trigger() {
          $(".cards_acessorios ul li article .add").trigger("click");
        }
        trigger();
      }
    },

    compre_junto: function () {
      //HÝ PRODUTOS PARA COMPRE JUNTO?
      if ($(".box_compre_junto .prateleira li").length != 0) {
        $(".box_compre_junto").removeClass("dis-none");

        //URL PARA CHECKOUT - var buy
        if (skuJson.skus.length === 1) {
          var buy =
            "/checkout/cart/add?sku=" +
            skuJson.skus[0].sku +
            "&qty=1&seller=1&";
        } else {
          if (selectedToBuy[0] === undefined) {
            $(".box_compre_junto .column_2 article .buy").addClass("disabled");
          } else {
            $(".box_compre_junto .column_2 article .buy").removeClass(
              "disabled"
            );
            var buy =
              "/checkout/cart/add?sku=" + selectedToBuy[0] + "&qty=1&seller=1&";
          }
        }

        //VALOR FINAL
        function total() {
          var total = 0;
          //VALOR: PRODUTO DA PÝGINA
          $(skuJson.skus).each(function (a, b) {
            if (b.available === true) {
              total = total = b.bestPrice;
              $(".box_compre_junto .column_2 article p strong").text(
                "R$ " + format_real(b.bestPrice)
              );
            }
          });

          //VALOR: PRODUTOS DO COMPRE JUNTO
          $(".box_compre_junto .prateleira ul li.adicionado article").each(
            function (a, b) {
              let data_id = $(b).data("id");
              vtexjs.catalog
                .getProductWithVariations(data_id)
                .done(function (product) {
                  total = total += product.skus[0].bestPrice;
                  $(".box_compre_junto .column_2 article p strong").text(
                    "R$ " + format_real(total)
                  );
                  buy = buy +=
                    "sku=" + product.skus[0].sku + "&qty=1&seller=1&";
                  $(".box_compre_junto .column_2 article .buy").attr(
                    "href",
                    buy
                  );
                });
            }
          );
        }
        total();

        //ADD
        $(".box_compre_junto .prateleira ul li article .add").on(
          "click",
          function (event) {
            event.preventDefault();
            console.log("ok");
            $(this).addClass("active");
            $(this).text("Adicionado");
            $(this).parents("li").addClass("adicionado");
            total();
          }
        );
      }
      setTimeout(() => {
        $(".compre_junto .add").each(function (index, element) {
          $(this).click();
        });
      }, 1000);
    },

    avise_me: function () {
      $("#notifymeButtonOK").val("Enviar");
    },

    descontoAVista: function () {
      var valorInicial = $(".preco-a-vista .skuPrice").text();
      valorInicial = valorInicial.replace(/[^\d]/gi, "");
      valorInicial = parseFloat(valorInicial);
      valorInicial = valorInicial / 100;
      valorDesconto = valorInicial * 0.95;
      valorDesconto = valorDesconto.toFixed(2).replace(".", ",").toString();
      if (valorDesconto.length > 6) {
        valorDesconto = valorDesconto.replace(
          /([0-9]{3}),([0-9]{2}$)/g,
          ".$1,$2"
        );
      }
      $(".preco-a-vista .skuPrice").text("R$ " + valorDesconto);
    },

    fraseDescontoAVista: function () {
      if (vtxctx.skus != "199" && vtxctx.skus != "214") {
        $(".descricao-preco").append(
          "<span style='margin-top: 7px; display:block;'>ou à vista com <b>5% de desconto no boleto</b></span>"
        );
      }
    },

    msgMotossera: function () {
      if (vtxctx.categoryName == "MOTOSSERRAS") {
        $(".product_info").append(
          '<img style="margin-top: 20px;" src="/arquivos/msgMotoserra.png" />'
        );
      }
    },

    msgAutomower: function () {
      if (vtxctx.skus == "214") {
        $(".descricao-preco").append(
          '<div><p style="margin-top: 10px; color: #5E5E5E; font-size: 13px; font-weight: bold">*Instalação Grátis</p><p style="margin-top: 10px; color: #5E5E5E; font-size: 13px; font-weight: bold">*Ideal para equipamentos de até 600m²</p></div>'
        );
      }
      if (vtxctx.skus == "199") {
        $(".descricao-preco").append(
          '<p style="margin-top: 10px; color: #5E5E5E; font-size: 13px; font-weight: bold">*Valor não inclui instalação do equipamento</p>'
        );
      }
    },

    msgMultifuncionais: function () {
      if (vtxctx.categoryName == "MULTIFUNCIONAIS") {
        $(".descricao-preco").append(
          '<p style="margin-top: 10px; color: #5E5E5E; font-size: 13px; font-weight: bold">*Acessórios vendidos separadamente</p>'
        );
      }
    },
  };

  if ($("body").hasClass("produto")) {
    ambos.parcelamento();
    ambos.detalhes();
    ambos.tabs();
    ambos.video();
    ambos.acessorios();
    ambos.ficha_tecnica();
    ambos.compre_junto();
    ambos.avise_me();
    ambos.msgMotossera();
    ambos.msgAutomower();
    // ambos.descontoAVista();
    ambos.fraseDescontoAVista();
    ambos.msgMultifuncionais();
    // ambos.buttonSimulador();
    $.ajax({
      url:
        "/api/catalog_system/pub/products/search?fq=productId:" +
        skuJson.productId,
      type: "GET",
      headers: header,
    }).done(function (response) {
      ambos.buttons(response);
    });
  }
})();

var departamento = (function () {
  var ambos = {
    best_price: function () {
      $(".prateleira .best_price").each(function (index, item) {
        var valorInicial = $(item).text();
        valorInicial = valorInicial.replace(/[^\d]/gi, "");
        valorInicial = parseFloat(valorInicial);
        valorInicial = valorInicial / 100;
        valorDesconto = valorInicial * 0.95;
        valorDesconto = valorDesconto.toFixed(2).replace(".", ",").toString();

        if (valorDesconto.length > 6) {
          valorDesconto = valorDesconto.replace(
            /([0-9]{3}),([0-9]{2}$)/g,
            ".$1,$2"
          );
        }

        $(item).text("R$ " + valorDesconto);
      });
    },

    flag_brinde: function () {
      $(".prateleira article").each(function (index, element) {
        var prodId = $(this).data("id");
        $.ajax({
          url: "/api/catalog_system/pub/products/search?fq=productId:" + prodId,
          type: "GET",
        }).done(function (response, status) {
          if (response[0].productClusters[141]) {
            $(element).append(
              '<img src="/arquivos/selo_brinde.png" alt="Flag Brinde" style="position:absolute; right:15px; top:15px; width: 80px">'
            );
          }
        });
      });
    },

    smart_research: function () {
      $('.search-multiple-navigator input[type="checkbox"]').vtexSmartResearch({
        shelfCallback: function () {
          console.log("shelfCallback");
          // ambos.best_price();
          // ambos.flag_brinde();
        },

        ajaxCallback: function () {
          console.log("ajaxCallback");
          // ambos.best_price();
          // ambos.flag_brinde();
        },
      });
    },

    filtros: function () {
      $(".departamento .search-multiple-navigator fieldset h5").on(
        "click",
        function () {
          $(this).toggleClass("active");
          $(this).next("div").slideToggle();
        }
      );
    },

    produtos_encontrados: function () {
      $(".departamento #product_length span").html(
        $(
          ".searchResultsTime:first-child .resultado-busca-numero .value"
        ).text()
      );
    },

    tags: function () {
      //ADD
      $(document).on(
        "change",
        ".search-multiple-navigator label input",
        function () {
          let thisName = $(this).parent().text(),
            thisClass = $(this).parent().attr("title"),
            categoriaSelecionada =
              '<li data-name="' +
              thisClass +
              '"><p>' +
              thisName +
              "</p><span>x</span></li>";

          if ($(this).parent().hasClass("sr_selected")) {
            $(".departamento .tags ul").append(categoriaSelecionada);
          } else {
            $(
              '.departamento .tags ul li[data-name="' + thisClass + '"]'
            ).remove();
          }

          //FECHA OPÇÕES
          $(this).parents("fieldset").find("h5").trigger("click");
        }
      );

      //REMOVE
      $(document).on("click", ".departamento .tags li", function (e) {
        e.preventDefault();
        $(
          '.search-multiple-navigator label[title="' +
            $(this).data("name") +
            '"]'
        ).trigger("click");
      });

      //CLEAR
      $(".departamento .tags .clear").on("click", function () {
        $(".departamento .tags ul li").trigger("click");
      });
    },
  };

  var mobile = {
    filtros: function () {
      function close() {
        //CLOSE - FILTROS
        $(
          ".departamento .navigation-tabs, .departamento .main .sub .orderBy"
        ).prepend('<span class="close_filtros">X</span>');
        $(".departamento .close_filtros").on("click", function (event) {
          event.preventDefault();
          $(
            ".departamento .navigation-tabs, .departamento .orderBy, #overlay, body"
          ).removeClass("active");
        });
      }
      close();

      function open(element) {
        $(element).addClass("active");
        $("#overlay, body").addClass("active");
      }

      //TIPOS, TIPO DE USO, CILINDRADA, ETC
      $(".departamento .open_filtros").on("click", function (event) {
        event.preventDefault();
        open(".departamento .navigation-tabs");
      });

      //ORDEM DE PRIORIDADE - PREÇO, RELEVANCIA, ETC
      $(".departamento .open_ordenacao").on("click", function (event) {
        event.preventDefault();
        open(".departamento .orderBy");
      });
    },
  };

  // ambos.best_price();

  if ($("body").hasClass("departamento")) {
    ambos.smart_research();
    ambos.filtros();
    ambos.produtos_encontrados();
    ambos.tags();

    if ($("body").width() < $mobile) {
      mobile.filtros();
    }
  }
})();

var institucional = (function () {
  var ambos = {
    question: function () {
      $(".institucional .content_perguntas>section .topic").on(
        "click",
        function () {
          $(this).toggleClass("active");
          $(this).next(".content").slideToggle();
        }
      );

      $(
        ".institucional .content_perguntas>section .content .item .question"
      ).on("click", function () {
        $(this).toggleClass("active");
        $(this).next(".text").slideToggle();
        $(this).parents(".item").toggleClass("active");
      });
    },

    form_message: function () {
      $(".institucional #form_message").submit(function (event) {
        event.preventDefault();

        $('#form_message input[type="submit"]').val("Aguarde...");

        let obj = {
          nome: $('#form_message input[name="nome"]').val(),
          email: $('#form_message input[name="email"]').val(),
          telefone: $('#form_message input[name="telefone"]').val(),
          assunto: $(
            '#form_message select[name="assunto"] option:selected'
          ).val(),
          numeroPedido: $('#form_message input[name="numero-pedido"]').val(),
          mensagem: $('#form_message textarea[name="mensagem"]').val(),
        };

        let json = JSON.stringify(obj);

        console.log(json);

        insertMasterData("FC", "husqvarna", json, function (res) {
          $("#form_message").trigger("reset");
          $('#form_message input[type="submit"]').val("Enviar Mensagem");
          $(".institucional .form_message.active .close").trigger("click"); //FECHA VERS�O POP UP
          swal(
            "Sua mensagem foi enviada!",
            "Em breve retornaremos o contato.",
            "success"
          );
        });
      });
    },

    open_form_message: function () {
      $(".institucional .open_form_message a").on("click", function (event) {
        if ($("body.perguntas-frequentes").length === 0) {
          event.preventDefault();
          $(".institucional .form_message, #overlay, body").addClass("active");
        }
      });

      $(".institucional .form_message .close").on("click", function () {
        $(".institucional .form_message, #overlay, body").removeClass("active");
      });
    },

    manuais: function () {
      $("#manuais").submit(function (event) {
        event.preventDefault();

        $('#manuais input[type="submit"]').addClass("disabled");

        $.ajax({
          type: "GET",
          dataType: "json",
          url: "/files/list_manuais.js",
        }).done(function (list) {
          var count = 0;
          $("body.manuais .result > ul li").remove();
          $('#manuais input[type="submit"]').removeClass("disabled");
          $("body.manuais .result .index").text(
            "Encontramos " + count + " resultados"
          );

          $.each(list, function (index, item) {
            let name = item.name.toLowerCase();
            let value = $('#manuais input[type="text"]').val().toLowerCase();

            if (name.indexOf(value) > -1 || name === value) {
              count = count += 1;
              $("body.manuais .result .index").text(
                "Encontramos " + count + " resultados"
              );

              let content = "";
              content += "<li>";
              content += "<p>";
              content +=
                '<a href="' +
                item.url +
                '" target="_blank" alt="' +
                item.name +
                '">';
              content +=
                '<img src="/arquivos/PDF_icon.png" alt="' + item.name + '" />';
              content += "</a>";
              content += "<span>" + item.name + "</span>";
              content += "</p>";
              content += "</li>";

              $("body.manuais .result > ul").append(content);
            }
          });
        });
      });
    },
  };

  var mobile = {
    list: function () {
      $(".institucional main .list .title").on("click", function () {
        $(this).next("ul").slideToggle();
        $("#overlay").toggleClass("active");
      });
    },
  };

  ambos.question();
  ambos.form_message();
  ambos.open_form_message();
  ambos.manuais();

  if ($("body").width() < $mobile) {
    mobile.list();
  }
})();

var erro = (function () {
  var ambos = {
    busca_erro: function () {
      $("#busca_erro").submit(function (event) {
        event.preventDefault();

        let texto = $('#busca_erro input[type="text"]').val();
        location.href = "/" + texto;
      });
    },
  };

  ambos.busca_erro();
})();

var Zoom = (function () {
  var mobile = {
    styleZoom: function () {
      var style = document.createElement("style");
      style.innerHTML = `
				body.zoom{
					overflow: hidden;
				}
				.mobile-zoom {
					justify-content: center;
					position: fixed;
					top: 0;
					left: 0;
					right: 0;
					bottom: 0;
					z-index: 9999;
				}
				.mobile-zoom .overlay{
					position:absolute;
					top: 0px;
					background: rgba(0, 0, 0, 0.75);
					height: 100%;
					width: 100%;
					display:block;
				}
				.mobile-zoom img {
					position: relative;
					top: 50%;
					transform: translateY(-50%);
					z-index; 2;
					padding: 0 15px;
				}
				.mobile-zoom span{
					color: #fff;
					font-weight: bold;
					top: 25px;
					position: absolute;
					right: 15px;
				}
			`;
      document.head.appendChild(style);
    },
    createZoom: function () {
      $("body").append(
        '<div class="mobile-zoom"><div class="overlay"></div><span class="close">X FECHAR</span><img src="" alt=""></div>'
      );
    },
    openZoom: function (src) {
      mobile.createZoom();
      console.log(src);
      $(".mobile-zoom img").attr("src", src);
    },
    removeZoom: function () {
      $(".mobile-zoom").remove();
    },
    clicks: function () {
      $(".apresentacao .slick-slide").click(function (e) {
        $("body").addClass("zoom");
        var src = $(this).find("img").attr("src");
        mobile.openZoom(src);
        $(".mobile-zoom .close").click(function (e) {
          mobile.removeZoom();
          $("body").removeClass("zoom");
        });
        $(".mobile-zoom .overlay").click(function (e) {
          mobile.removeZoom();
          $("body").removeClass("zoom");
        });
      });
    },
  };
  if ($("body").is(".produto") && $(window).width() < 768) {
    $(document).ready(function () {
      mobile.styleZoom();
      setTimeout(() => {
        mobile.clicks();
      }, 1500);
    });
  }
})();
