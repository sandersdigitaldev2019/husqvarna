var header = {
    Accept: "application/json",
    "REST-range": "resources=0-1000",
    "Content-Type": "application/json; charset=utf-8",
};

var insertMasterData = function (ENT, loja, dados, fn) {
    $.ajax({
        url: "/api/dataentities/" + ENT + "/documents",
        type: "PATCH",
        data: dados,
        headers: header,
        success: function (res) {
            fn(res);
        },
        error: function (res) {
            swal(
                "Oops!",
                "Houve um problema. Tente novamente mais tarde.",
                "error"
            );
        },
    });
};

$(document).ready(function () {
    console.log('SimuladorPage');
    $('.input-value').html( $('#simulador-range').val() + 'm²');
    $('#simulador-range').on('change',function() {
        this.setAttribute('value',this.value);
        $('.input-value').html( this.value + 'm²' );
    });
    $('.button.next').click(function (e) { 
        $('.simulador_display div.active').removeClass('active');
        e.preventDefault();
    });
    $('.button.back').click(function (e) { 
        var data = $(this).attr('class')
        data = data.replaceAll('button back p', '');
        $('.simulador_display .paineis div').removeClass('active');
        if(data == 01){
            $('.simulador_display .paineis').removeClass('active-02');
            $('.simulador_display .painel-resumo').removeClass('active');
            $('.simulador_display .painel-resultado').removeClass('active');
            $('.simulador_display .painel-resultado').removeClass('box1');
            $('.simulador_display .painel-resultado').removeClass('box2');
            $('.painel-intro').addClass('active');
            $('.paineis').addClass('active');
        }else if(data == 02){ 
            $('.simulador_display .paineis').removeClass('active-03');
            $('.painel-area').addClass('active');
            $('.paineis').addClass('active active-02');
        }else if(data == 03){
            $('.simulador_display .paineis').removeClass('active-04');
            $('.painel-complexidade').addClass('active');
            $('.paineis').addClass('active active-03');
        }
    });
    $('.button.next.p02').click(function (e) { 
        $('.paineis').removeClass('active-01');
        $('.paineis').addClass('active active-02');
        $('.painel-area').addClass('active');
    });
    $('.button.next.p03').click(function (e) { 
        $('.simulador_display .paineis').removeClass('active-02');
        $('.paineis').addClass('active active-03');
        $('.painel-complexidade').addClass('active');
        $('#simulador-tamanho').val($('#simulador-range').val());
    });
    $('.painel-complexidade .list-simulador li').click(function (e) {  
        $('.simulador_display .paineis').removeClass('active-03');
        $('#simulador-complexidade').val($(this).index())  
        $('.paineis').addClass('active active-04');
        $('.painel-complexidade').removeClass('active');
        $('.painel-inclinacao').addClass('active');  
    });
    $('.painel-inclinacao .list-simulador').click(function (e) { 
        $('#simulador-inclinacao').val($(this).index()) 
        $('.painel-inclinacao').removeClass('active'); 
        $('.painel-resumo').addClass('active');  
        $('.paineis').removeClass('active'); 
        $('.paineis').removeClass('active-04');
    });
    $('.button.next.p06').click(function (e) { 
        getTam = $('#simulador-tamanho').val()
        getCom = $('#simulador-complexidade').val()
        getInc = $('#simulador-inclinacao').val()
        header = {
            "accept": "application/json",
            "rest-range": "resources=0-300",
            'Content-Type': 'application/json; charset=utf-8'
        };
        if(getTam <= 600 && getCom == 0 && getInc == 5){
            $('.painel-resultado').addClass('active box1');
            productId = 200;
        }else{
            $('.painel-resultado').addClass('active box2');  
            productId = 199;
        }
        $.ajax({
            url: "/api/catalog_system/pub/products/search?fq=productId:" + productId,
            type: 'GET',
            headers: header
        }).
        done(function (response) {
            price = response[0].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
            price = price.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            price = price.replace(',', '-').replace('.', ',').replace('-', '.')
            if(productId == 200){
                $('.painel-resultado table .am105 td:nth-child(2)').html('R$ ' + price);
            }
            if(productId == 199){
                $('.painel-resultado table .am310 td:nth-child(2)').html('R$ ' + price);
            }
            console.log(response[0].items[0].sellers[0].commertialOffer.PriceWithoutDiscount);
            
        })
        $('.painel-resumo').removeClass('active'); 
        $('.painel-resultado').addClass('active');  

    });
    $('h3.help').click(function (e) { 
        $(this).parent().find('.help_display').toggleClass('open');        
    });
    $('.go_to_automower').click(function (e) { 
        swal(
            "Sua área é maior que 1.000 m²?",
            "Na Loja Husqvarna temos disponíveis somente 2 modelos, mas não se preocupe que temos o modelo ideal para o seu gramado, uma de nossas Revendas Especializadas irá entrar em contato com você para um orçamento dedicado. Finalize a simulação em www.automower.com.br",
            "info"
        ).then(function() {
            window.location = "https://www.automower.com.br";
        });
    });
    $('.painel-form input[type="submit"]').click(function (e) { 
        var nome = $(".form .name").val();
        var email = $(".form .email").val();
        var telefone = $(".form .tel").val();
        var cep = $(".form .cep").val();
        var cidade = $(".form .cidade").val();
        if( nome && email && telefone && cep && cidade ){
            var infos = {
                "nome" : nome,
                "email" : email,
                "telefone" : telefone,
                "cep" : cep,
                "cidade" : cidade
            }
            let json = JSON.stringify(infos);
            insertMasterData("SA", "husqvarna", json, function (res) {
                console.log(res);
                $('.simulador_display div.active').removeClass('active');
                $('.paineis').addClass('active active-01');
                $('.painel-intro').addClass('active');

            });
        }else{
            swal(
                "Opps!",
                "todos os campos precisam ser preenchidos",
                "error"
            );
        }
    });

});

